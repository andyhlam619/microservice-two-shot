function ShoeList(props) {
	const deleteShoe = async (id) => {
		const shoesUrl = `http://localhost:8080/api/shoes/${id}`;
		const fetchConfig = {
			method: "delete",
			headers: {
				"Content-Type": "application/json",
			},
		};

		const response = await fetch(shoesUrl, fetchConfig);
		if (response.ok) {
			window.location.reload();
		}
	};
	return (
		<div className="container mt-5">
			<div className="row row-cols-1 row-cols-md-2 g-4 t-5 m-2">
				{props.shoes.map((shoe) => {
					return (
						<div key={shoe.id} className="col">
							<div className="card-body mb-3 shadow">
								<h5 className="card-header">{shoe.manufacturer}</h5>
								<div className="card-body">
									<h5 className="card-title">{shoe.model_name}</h5>
									<img src={shoe.picture_url} className="card-img-top" />
									<ul className="list-group list-group-flush">
										<li className="list-group-item">{shoe.color}</li>
										<li className="list-group-item">{shoe.bin.closet_name}</li>
									</ul>
									<div className="col text-center">
										<button className="btn btn-primary m-2">Details</button>
										<button
											onClick={(e) => deleteShoe(shoe.id)}
											className="btn btn-secondary">
											Delete
										</button>
									</div>
								</div>
							</div>
						</div>
					);
				})}
			</div>
		</div>
	);
}

export default ShoeList;
