import React, {useEffect, useState} from 'react'

function HatsForm(props) {
///// This chunk of code handles the submit of the hat form//////
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.fabric = fabric;
        data.style_name = style_name;
        data.color = color;
        data.picture_url = picture_url;
        data.location = location;
        console.log(data);

        const hatsUrl = "http://localhost:8090/api/hats/";
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(hatsUrl, fetchConfig);
        if (response.ok) {
          const newhats = await response.json();
          console.log(newhats);
          setFabric("");
          setStyle("");
          setColor("");
          setUrl("");
          setLocation("");
          props.fetchHats();
        }
      }
// //sets the values of the states depending on the value in our respective inputs///////////////////////////////////////////////////////////////////////////
// //so when we type into each input, the state will update to what we wrote///////////////////////////////////////////////////////////////////////////
    const [fabric, setFabric] = useState('');
    const handleFabricChange = (event) => {
      const value = event.target.value;
      setFabric(value);
    }

    const [style_name, setStyle] = useState('');
    const handleStyleChange = (event) => {
      const value = event.target.value;
      setStyle(value);
    }

    const [color, setColor] = useState('');
    const handleColorChange = (event) => {
      const value = event.target.value;
      setColor(value);
    }

    const [picture_url, setUrl] = useState('');
    const handleURLChange = (event) => {
      const value = event.target.value;
      setUrl(value);
    }

    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
      const value = event.target.value;
      setLocation(value);
    }


/////this gets our locations to show up on the drop down menu///////////////////////////////////////////////////////////////////////////
    const [locations, setLocations] = useState([]);
    const fetchData = async () => {

    const url = 'http://localhost:8100/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        setLocations(data.locations)
        }

    }
    useEffect(() => {
    fetchData();
    }, []);
//our jsx///////////////////////////////////////////////////////////////////////////
  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a hat!</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input value={fabric} onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input value={style_name} onChange={handleStyleChange} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control"/>
                <label htmlFor="style_name">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={color} onChange={handleColorChange} placeholder="Color" type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input value={picture_url} onChange={handleURLChange} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                <label htmlFor="picture_url">Picture URL</label>
              </div>
              <div className="mb-3">
                <select value={location} onChange={handleLocationChange} required id="location" name="location" className="form-select">
                  <option value="">Choose a Location</option>
                  {locations.map(location => {
                        return (
                            <option key={location.id} value={location.href}>
                            {location.closet_name}
                            </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
  )
}

export default HatsForm
