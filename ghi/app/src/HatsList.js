import React, {useEffect, useState} from 'react'

function HatsList(props) {
/////This code refers to the delete button. You will see an onclick listener inside our jsx.//
///// This is so when we delete a "hat" it automatically "refreshes" the page for us.
    const deleteItem = async(hat) => {
        const hatUrl = `http://localhost:8090${hat.href}`;
        const fetchConfig = {
          method: 'delete',
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(hatUrl, fetchConfig);
        console.log(response)
        // This is the two lines of code that refreshes our page for us
        if (response.ok) {
            props.fetchHats();
        }
    }

//Our JSX that renders our table
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Location</th>
                    <th>Remove</th>
                </tr>
            </thead>
            <tbody>
                {/* This code is basically a "for-loop" but in JSX. It pretty much
                fills out the rows of our table with our data by passing through the prop. See
                in the app.js  */}
                {props.hats.map(hat => {
                return (
                    <tr key={hat.href}>
                        <td>{ hat.fabric }</td>
                        <td>{ hat.style_name }</td>
                        <td>{ hat.color }</td>
                        <td>{ hat.location }</td>
                        <td><button type="button" className="btn btn-danger" onClick={() => deleteItem(hat)}>Delete!</button> </td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
    );
  }

export default HatsList;
