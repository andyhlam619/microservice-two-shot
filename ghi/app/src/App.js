import { BrowserRouter, Routes, Route } from 'react-router-dom';

import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import HatsForm from './HatsForm';
import ShoeList from "./ShoeList"
import ShoeForm from "./ShoeForm"
import React, {useEffect, useState} from 'react'


function App() {
////Fetching hats and shoes data here.
///!!!!!!!!I already created the fetch for shoes already!!!!!////
  const [hats, setHats] = useState([]);
  const [shoes, setShoes] = useState([]);

  const fetchHats = async () => {
    const url = 'http://localhost:8090/api/hats/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setHats(data)
      }
  }

  const fetchShoes = async () => {
    const url = 'http://localhost:8080/api/shoes/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setShoes(data)
      }
  }

////Using the states////
  useEffect(() => {
      fetchHats();
      fetchShoes();
  }, []);

////The JSX////
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats" >
            <Route path="" element={<HatsList hats={hats} fetchHats={fetchHats}/>} />
            <Route path="new" element={<HatsForm fetchHats={fetchHats}/>} />
          </Route>
          <Route path="shoes" >
            <Route path="" element={<ShoeList shoes={shoes} fetchShoes={fetchShoes}/>} />
            <Route path="new" element={<ShoeForm fetchShoes={fetchShoes}/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
